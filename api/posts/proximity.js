const request = require('request-promise-native');

const Deg2Rad = deg => deg * Math.PI / 180;

const PythagorasEquirectangular = (lat1, lon1, lat2, lon2) => {
    lat1 = Deg2Rad(lat1);
    lat2 = Deg2Rad(lat2);
    lon1 = Deg2Rad(lon1);
    lon2 = Deg2Rad(lon2);
    const R = 6371; // km
    const x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
    const y = (lat2 - lat1);
    const d = Math.sqrt(x * x + y * y) * R;
    return d;
};

const minDiff = 1000;
const isNear = (lat, lng, diff = minDiff) => {
    return (user) => {
        const userLat = user.address.geo.lat;
        const userLng = user.address.geo.lng;
        const userDiff = PythagorasEquirectangular(lat, lng, userLat, userLng);
        return userDiff < diff;
    }
};

module.exports = async function(req, res, next) {
    if (req.method !== 'POST') return res.status(405).set('Allow', 'POST').send({err: 'invalid method, required POST'});
    const {geo} = req.body;
    if (!geo || !geo.lat || !geo.lng) {
        return res.status(400).send({err: "bad request"});
    }
    try {
        const users = await request.get({ url: 'https://jsonplaceholder.typicode.com/users', json: true });
        const idArr = users.filter(isNear(geo.lat, geo.lng)).map((user) => {return user.id});

        const posts = await request.get({ url: 'https://jsonplaceholder.typicode.com/posts', json: true });
        const result = {
            "data": posts.filter((post) => {
                return idArr.includes(post.userId);
            })
        };

        return res.send(result);
    } catch (e) {
        console.error(e);
        return  res.status(500).send({err: "internal error"});
    }
};