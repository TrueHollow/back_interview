const express = require('express');

const app = express();
app.listen(3000);

app.use(require('body-parser').json());
app.use('/posts/proximity', require('./api/posts/proximity'));

console.log('app running on port 3000...');

module.exports = app;
