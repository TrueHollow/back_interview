const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../app');

describe('POST /posts/proximity', () => {
	it('should test geo', async() => {
		const data = {
			"geo": {
				"lat": "-43.1234",
				"lng": "-34.1234"
			}
		};
		let res = await chai.request(app).post('/posts/proximity').send(data);
		expect(res).to.have.status(200);
	});

});
