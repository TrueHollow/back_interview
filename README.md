## Preparation
Installing dependencies:

```sh
$ npm install
```

## Run
Running app

```sh
$ npm start
```

## Tests
Running tests

```sh
$ npm test
```
